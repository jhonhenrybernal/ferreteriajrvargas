<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/sales', 'SalesController@index')->name('sales.index');

Route::post('sales/cellar/', 'SalesController@sspCellarAll');

Route::post('/list/cellar', 'SalesController@sspListCellar');

Route::post('/image/cellar', 'SalesController@sspImageCellar');

Route::get('/list/admin/discount', 'SalesController@sspAdminDiscount');

//Route::get('/list/admin/client', 'SalesController@sspClient');

Route::post('/admin/discount/request', 'SalesController@sspDiscoutRequest');


Route::post('/admin/discount/refresh', 'SalesController@sspDiscountStatus');

Route::post('/admin/searh/client', 'SalesController@sspSearchClient');

Route::post('/admin/select/client', 'SalesController@sspClientSelect');

Route::post('/admin/client/create', 'SalesController@sspClientCreate');

Route::post('/admin/history/credit', 'SalesController@sspClientCreditHistory');

Route::post('/process/sale', 'SalesController@saleProcess');
