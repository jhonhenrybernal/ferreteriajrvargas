@extends('layouts.app')
@section('content')
<div class="content-wrapper" style="min-height: 476px;">
           <!-- Main content -->
      <section class="content">
        <div class="row">
            <div class="col-md-5">
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title">Lista seleccionado</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table class="table table-bordered" id="list-cellar-selected">
           
                    <thead>
                        <tr>
                            <th style="width: 2px">Codigo</th>                           
                            <th style="width: 218px">Nombre</th>
                            <th style="width: 2px">Cant</th>
                            <th style="width: 73px">Precio u</th>
                            <th style="width: 105px">Total unitario</th>
                             <th></th>    
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>

                    
                    
                </tbody></table>
                 <table border="1" align="right" >
                <tr>
                   
                </tr>
                <tr>
                    <td>SubTotal</td>
                    <td style="width: 80px" id="subtotal"></td>
                </tr>
                <tr>
                    <td>Descuento</td>
                    <td id="descuento"></td>
                </tr>
                <tr>
                    <td>Iva</td>
                    <td id="iva"></td>
                </tr>
                <tr>
                    <td >Total</td>
                    <td id="total"></td>
                </tr>
                </table>
                </div>
                <form method=post id="post-sale">
                  <button type="button" class="btn btn-block btn-warning  btn-xs" id="submit-sale">Venta</button>    
                </form>
            </div>
         
            </div>
            <!-- /.col -->
            <div class="col-md-7">
              <div class="box" >               
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="box-body">
                      <div class="btn-group"  id="admin-client">
                      <input type="hidden" id="id_client" value="">
                      <input type="hidden" id="status_client" value="">
                        <button type="button" class="btn btn-default" id='client-init' onclick="clientsModal()"></button> 
                      </div>
                      <div class="btn-group" id="admin-discount" style="width: 19%">
                       
                                          
                      </div>
                      <input type="hidden" id="apply-discount-final" value="">                                
                      <input type="hidden" id="value-discount-final" value="">
                      <div class="btn-group" id="seletQuotation">
                      
                      </div>
                      <div class="btn-group">
                      <select class="form-control" id="type-bill">
                            <option >Tipo de factura</option>
                            <option value="fis">Fisica</option>
                            <option value="vir">Virtual</option>
                        </select>
                      </div>                          
                  </div>
                    <div class="box-body" id="all-credit-client">
                       
                    </div>              
                    <div class="box-body">
                      <div class="col-xs-3"></div>
                          <div class="row">                        
                              <div class="col-xs-6">
                                  <input type="text" class="form-control" placeholder="Busque el producto" id="search-cellar">
                            
                              </div>
                          </div>
                      </div>
                      <div class="box-body">
                          <div class="row" id='list-cellar-detaill'>
                                                             
                          </div>
                      </div>


                      <div class="box-body">
                          <div class="row" id='list-cellar-view'>                        
                          
                          </div>
                          <div class="col-xs-3"></div>                        
                          <div class="col-xs-4" id='list-total-cellar-view'>
                             
                          </div>
                      </div>
                      <div class="box-body" id='list-cellar-view'>
                       
                      </div>
                   </div>
                </div>
            <!-- /.box -->

            <div class="box">
                <div class="box-header">
                <h3 class="box-title" >Lista bodega 
                <button type="button" onClick="refreshCellar()" class="btn btn-default btn-xs" aria-label="Left Align">
                <span  class="glyphicon glyphicon-repeat" aria-hidden="true"></span>
                Refrescar
                </button></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">

                <table id="cellar-table"  class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width: 10px">Codigo</th>
                            <th style="width: 100%">Nombre</th>
                            <th style="width: 5px">Cant</th>
                            <th style="width: 5px">Iva</th>
                            <th>Precio venta</th>
                            <th style="width: 70px">Desc</th>
                            <th>Total publico</th>
                            <th></th>                             
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
               
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->       
      </section>
      <!-- /.content -->
    </div>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close-image">&times;</span>
    <div class="box-body">
        <div class="carousel-cellar">
            <div id="imagen-cellar"></div>
            <input type="hidden" id="cant-img-cellar" value="">
        </div>
     <div class="box-body">
        <div class="carousel-cellar-an">
            <button  onClick="retrocederFoto()">+</button>
            <input type="text" style="width : 25px;" value="" disabled>
            <button  onClick="pasarFoto()" >-</button>
        </div>
    </div>
    </div>
  </div>
  
  </div>


  <!-- The Modal -->
<div id="modal-client" class="modal">
  <!-- Modal content -->
      <div class="modal-content-client">
        <span class="close-client">&times;</span>       
            <div class="box-body"> 
                <div id="alert_client" style='width: 400px;'></div>
                 <div class="col-xs-6"><select id='selUser' style='width: 100px;'>
                </select></div>
                <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-primary" onclick="createClient()" >Crear cliente
                    </button>
                </div>                
            </div>
            <div class="modal-body"  id="client-form">
            <form id="form" name="form">
               <div class="box box-success">
                   <div class="row">
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Tipo de documento</label>
                                <select class="form-control" id="type_document">
                                <option>Seleccione</option>
                                <option value="ci">Cedula de identidad</option>
                                <option value="cj">Cedula juridica</option>
                                <option value="dx">Dimex</option>
                                <option value="nt">Nite</option>
                              </select>
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Numero de documento</label>
                          <input type="text" class="form-control" placeholder="Numero de documento" id="num_document-client">
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-2">
                            <label for="exampleInputEmail1">Area</label>
                                <select class="form-control" id="area-client" >
                                    <option value="cr">Consta rica</option>
                                </select>
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-4">
                            <label for="exampleInputEmail1">Email</label>
                          <input type="text" class="form-control" placeholder="Email" id="mail-client">
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <div class="row">
                      
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Nombre</label>
                          <input type="text" class="form-control" placeholder="Nombre" id="name-client">
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Primer apellido</label>
                          <input type="text" class="form-control" placeholder="Primer apellido" id="Surname-client">
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Seguno apellido</label>
                          <input type="text" class="form-control" placeholder="Seguno apellido" id="second_surname-client">
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Nombre Comercial </label>
                          <input type="text" class="form-control" placeholder="Nombre Comercial" id="tradename-client">
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                     <div class="row">
                      
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Direccion</label>
                          <input type="text" class="form-control" placeholder="Direccion" id="addrers-client">
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Provincia</label>
                                 <select class="form-control" id="province-client">
                                    <option>Seleccione</option>
                                    <option value="limon">Limon</option>
                                  </select>
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Canton</label>
                                 <select class="form-control" id="canton-client">
                                    <option>Seleccione</option>
                                    <option value="central">Central</option>
                                  </select>
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Distrito</label>
                                 <select class="form-control" id="district-client">
                                    <option>Seleccione</option>
                                    <option value="catedral">Catedral</option>
                                  </select>
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
              </div>
              <button type="button" id="submitClientCreate" onclick="formCreateClient()"  name="data" class="btn btn-primary">Guardar</button>
              </form>
          </div>
        <div class="modal-body" id="client-view">
               <div class="box box-success">
                   <div class="row">
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Tipo de documento</label>
                            <table class="table table-bordered"> <tr><th id="type-document-client"></th></tr></table>                              
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Numero de documento</label>
                         <table class="table table-bordered"> <tr><th id="number-tdocument-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-2">
                            <label for="exampleInputEmail1">Area</label>
                               <table class="table table-bordered"> <tr><th id="area-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-4">
                            <label for="exampleInputEmail1">Email</label>
                          <table class="table table-bordered"> <tr><th id="mail-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <div class="row">
                      
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Nombre</label>
                          <table class="table table-bordered"> <tr><th id="name-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Primer apellido</label>
                         <table class="table table-bordered"> <tr><th id="surname-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Seguno apellido</label>
                          <table class="table table-bordered"> <tr><th id="ssurname-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Nombre Comercial </label>
                          <table class="table table-bordered"> <tr><th id="tradename-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                     <div class="row">
                      
                        <div class="col-lg-3">
                            <label for="exampleInputEmail1">Direccion</label>
                          <table class="table table-bordered"> <tr><th id="addrers-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Provincia</label>
                                <table class="table table-bordered"> <tr><th id="province-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Canton</label>
                                <table class="table table-bordered"> <tr><th id="canton-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                         <div class="col-lg-3">
                            <label for="exampleInputEmail1">Distrito</label>
                                <table class="table table-bordered"> <tr><th id="district-view-client"></th></tr></table>    
                          <!-- /input-group -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
              </div>
          </div>
            <!-- The Modal -->
          <div class="row" id="client-credit-history">
            <div class="col-lg-8">
              <div class="box">
              <div class="box-header">
                <h3 class="box-title">Historia crediticia</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding" id="table-credit-client">
                  
                </div>
                <!-- /.box-body -->
              </div>
          </div>  
        </div>  
      </div>
</div>   
    
  @endsection