const iva = 13;
/* panel administrador y buscar*/

  //refresca de productos en bodega 
function refreshCellar(){
    $('#cellar-table').DataTable().ajax.reload()
};


 $.ajaxSetup({

        headers: {

          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

      }

  });



//llama buscar
var tableCellar = $('#cellar-table').DataTable({
  responsive: true,
  searchDelay: 500,
  dom: 'Bfrtip',
  buttons: [

  ],

  responsive: {
      details: {
          type: 'column'
      }
  },
  columnDefs: [{
          className: 'control',
          orderable: false,
          targets: 0
      },
      {
          "targets": [],
          "visible": false
      }
  ],
  "pageLength": 15,
  "order": [[0, "asc" ]],
  language: {

        "decimal": "",
        "emptyTable": "Realizar busqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Buscando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    processing: true,
    serverSide: true,
    "ajax": {
          "url" : "/sales/cellar/",
         "type": "POST",
         "data" :function ( d ) {
          return $('#search-cellar').serialize()}
      },
   
    "columnDefs": [
      {"className": "dt-center", "targets": 3},
    ],
    columns:[
    {data: 'cod_cellar', name: 'cod_cellar'},
    {data: 'name', name: 'name'},
    {data: 'cant', name: 'cant'},
    {data: 'iva', name: 'iva'},
    {data: 'value_sale', name: 'value_sale'},
    { "data": null,
    render:function(data, type, row)
    {   
    	//return '<div id="discount-unit'+data['id'] +'"><div class="carousel-cellar-an"><input id="value-discount-unid'+data['id'] +'" class="form-control" type="number" value="0" style="width: 70px"><button  class="btn btn-secondary  btn-flat fa fa-check discountUnid"></button></div></div>';	   
      return '<div id="discount-unit'+data['id'] +'"><div class="carousel-cellar-an"><input id="value-discount-unid'+data['id'] +'" class="form-control" type="number" value="0" style="width: 70px"></div></div>';
    },
    "targets": -1},
    { "data": null,
    render:function(data, type, row)
    {
      return "<i id='cellar-id' data-cellar-name="+data['name'] +" data-cellar-cod="+data['cod_cellar'] +" data-cellar-descrip="+data['description']+" data-cellar-image="+data['image'] +" data-cellar-value-sale="+data['value_sale'] +">" + data['value_public']+"</i>";
    },
    "targets": -1},    
    {
      data: "cellar-table", "width": "50px", "render": function (data) {
          return '<button type="button" class="select btn btn-block btn-primary  btn-xs"><span class="glyphicon glyphicon-ok"></span></button>';
      }
    }],
    
  });

/*cotizacion*/
function selectQuotation(value){

  if (value == 1) {
    document.getElementById("seletQuotation").innerHTML = '' 
     var discount = '<button type="button"  class="btn btn-default" onclick="selectQuotation(0)" id="select-quotation">Venta</button>' 
  $("#seletQuotation").append(discount); 
  }else{
    document.getElementById("seletQuotation").innerHTML = '' 
   var discount = '<button type="button"  class="btn btn-default" onclick="selectQuotation(1)" id="select-quotation">Cotizacion</button>' 
  $("#seletQuotation").append(discount); 

  }
}

/*decuentos*/
function selectDiscount(){
  var client = document.getElementById("client-init").innerHTML; 

var descuento = document.getElementById("value-discount-final").value
document.getElementById('apply-discount-final').value = 'si';
document.getElementById("admin-discount").innerHTML = ''; 
var discount = '  <div class="input-group input-group-sm"><input type="text" class="form-control" value="Aplicado '+descuento+'%" disabled><span class="input-group-btn"><button type="button" class="btn btn-info btn-flat fa fa-plus-square" onClick="incrementDiscount()"></button></span></div>' 
      $("#admin-discount").append(discount); 
 
  
}

function incrementDiscount(){

  $('#requestDiscount').tooltip()
  document.getElementById("admin-discount").innerHTML = ''; 
  var discount = '  <div class="input-group input-group-sm"><input type="number" class="form-control" id="discount_value_select" value="" min="1" max="100" required><span class="input-group-btn"><button type="button" class="btn btn-info btn-flat fa fa-check" id="requestDiscount" onClick="requestDiscount()" title="Solictar aumento de decuento"></button></span></div>' 
    $("#admin-discount").append(discount); 
 }

function requestDiscount(){
  var discount = document.getElementById("discount_value_select").value;
  var idClient = document.getElementById("id_client").value;
  var discountDefault = document.getElementById("value-discount-final").value;

  if (parseInt(discount) <= parseInt(discountDefault)) {

	document.getElementById("admin-discount").innerHTML = ''
    var disscount = document.getElementById("value-discount-final").value = discount
    document.getElementById("apply-discount-final").value = 'si';
    var discountDom = '<button type="button" class="btn btn-default" onclick="incrementDiscount()">Des aplicado: '+discount+'%</button>' 
    $("#admin-discount").append(discountDom)
  
  }else{
    $.ajax({

      type:'post',

      url:"/admin/discount/request",

      data:{idClient:idClient,discount:discount},

        success:function(data){      
        //descuento 
          if (data.status == 1) {
            document.getElementById('admin-discount').innerHTML = ''
            var discount = '  <div class="input-group input-group-sm"><input type="text" class="form-control" id="status-discount" value="'+data.msm+'" disabled><span class="input-group-btn"><button type="button" class="btn btn-info btn-flat fa fa-refresh" onClick="refreshStatusDsicount('+data.id+')"></button></span></div>' 
            $("#admin-discount").append(discount)
          }else if (data.status == 4) {
            alert(data.msm)
          }
       
        }
    }); 
	
  }


}


//refrescar descuento todo
function refreshStatusDsicount(id){
  //var idClient = document.getElementById("id_client").value; 
  $.ajax({

    type:'post',

    url:"/admin/discount/refresh",

    data:{id:id},

      success:function(data){
        switch (data.status){
          case  1:
            if ($('#status-discount').length) {
              document.getElementById("status-discount").value = data.msm;
            }
        
          
          break;
          
          case 2:
            if ($('#status-discount').length) {
              document.getElementById("status-discount").value = data.msm;
            }
            
          
          document.getElementById("value-discount-final").value = data.discount;
          document.getElementById('apply-discount-final').value = 'si';
          setTimeout(function(){; 
            document.getElementById("admin-discount").innerHTML = ''; 
            var discount = '  <div class="input-group input-group-sm"><input type="text" class="form-control" value="Aplicado '+data.discount+'%" disabled><span class="input-group-btn"><button type="button" class="btn btn-info btn-flat fa fa-plus-square" onClick="incrementDiscount()"></button></span></div>' 
            $("#admin-discount").append(discount); 
          }, 5000);
          break;

          case 3:
            if ($('#status-discount').length) {
              document.getElementById("status-discount").value = data.msm;
            }
            
           
          document.getElementById("value-discount-final").value = data.discount;
          document.getElementById('apply-discount-final').value = 'si';
          setTimeout(function(){ ; 
            document.getElementById("admin-discount").innerHTML = ''; 
            var discount = '  <div class="input-group input-group-sm"><input type="text" class="form-control" value="Aplicado '+data.discount+'%" disabled><span class="input-group-btn"><button type="button" class="btn btn-info btn-flat fa fa-plus-square" onClick="incrementDiscount()"></button></span></div>' 
            $("#admin-discount").append(discount); 
          }, 7000);
          break;
        }
      }  
  }); 
  
}

function incrementDiscountUnit(){


}


// refrescar descuento por unidad 
function refreshStatusDsicountUnid(id,id_dom){
  document.getElementById("discount-unit"+id_dom).innerHTML = ''; 
  $.ajax({

    type:'post',

    url:"/admin/discount/refresh",

    data:{id:id},

      success:function(data){
        switch (data.status){
          case  1:
 
            var discount = '<button type="button" class="btn btn-default" onClick="refreshStatusDsicountUnid('+id+','+id_dom+')" id="msm-status-discount-unid" >'+data.msm+'&nbsp;<i class="fa fa-refresh"></i></button>'
            $("#discount-unit"+id_dom).append(discount); 
          
          
          break;
          
          case 2:
    
            var discount = '<input type="hidden" id="value-discount-unid'+id_dom+'" value="'+data.discount+'"> <button type="button" class="btn btn-default"  id="msm-status-discount-unid" >'+data.msm+'&nbsp;<i class="fa fa-check"></i></button>'
            $("#discount-unit"+id_dom).append(discount);
          break;

          case 3:

            if ($('#msm-status-discount-unid').length) {
              document.getElementById("msm-status-discount-unid").value = data.msm;
            }

          document.getElementById("value-discount-final").value = data.discount;
          document.getElementById('apply-discount-final').value = 'si';
          setTimeout(function(){ ; 
            document.getElementById("admin-discount").innerHTML = ''; 
            var discount = '  <div class="input-group input-group-sm"><input type="text" class="form-control" value="Aplicado '+data.discount+'%" disabled><span class="input-group-btn"><button type="button" class="btn btn-info btn-flat fa fa-plus-square" onClick="incrementDiscount()"></button></span></div>' 
            $("#admin-discount").append(discount); 
          }, 7000);
          break;
        }
      }  
  }); 
  
}

/*ver Clientes*/

function clientsModal(){

    document.getElementById("alert_client").innerHTML = ''; 
    var modalClient = document.getElementById("modal-client");
    var formClient = document.getElementById("client-form");
    modalClient.style.display = "block";
    formClient.style.display = "none";
    
  if (document.getElementById("status_client").value != 1) {
     
      var viewClient = document.getElementById("client-view");
      var viewClientCredit = document.getElementById("client-credit-history");
      viewClientCredit.style.display = "none";
      
      viewClient.style.display = "none";
  }

    


}

//crear cliente


function formCreateClient(){
  if (document.getElementById("client-init").innerHTML != 'Cliente: Sin seleccionar' ) {
    document.getElementById("client-init").innerHTML = "Cliente: Sin seleccionar";    
  }

   var tipoDocu = document.getElementById("type_document").value;
   var NumDocu = document.getElementById("num_document-client").value;
   var area = document.getElementById("area-client").value;
   var mail = document.getElementById("mail-client").value;
   var name = document.getElementById("name-client").value;
   var surname = document.getElementById("Surname-client").value;
   var ssurname = document.getElementById("second_surname-client").value;
   var tradename = document.getElementById("tradename-client").value;
   var addrers = document.getElementById("addrers-client").value;
   var cantor = document.getElementById("canton-client").value;
   var district = document.getElementById("district-client").value;
   var province = document.getElementById("province-client").value;
  $.ajax({
       type: 'POST',
       url: "/admin/client/create",
       data: {
        tipoDoc:tipoDocu,
        NumDocu:NumDocu,
        area:area,
        mail:mail,
        name:name,
        surname:surname,
        ssurname:ssurname,
        tradename:tradename,
        addrers:addrers,
        cantor:cantor,
        district:district,
        province:province

      }, 
       success: function(response) {
           var alert = '<div class="alert alert-success alert-dismissible" ><a href="#" class="close_alert">&times;</a><strong>Creado!</strong> Bienvenido (a): ' + response.name_full  +'</div>'
          $("#alert_client").append(alert)
           document.getElementById("client-init").innerHTML = "Cliente: " +response.name_full;  
           document.getElementById('id_client').value = response.id
       },
      error: function() {
          var alert = '<div class="alert alert-error alert-dismissible" ><a href="#" class="close_alert">&times;</a><strong>Ups!</strong> No se pudo crear</div>'
          $("#alert_client").append(alert)
      }
   });
}

//varias ejecuciones
$(document).ready(function() {
  //datos administrativos

  //descuento inicial 
  $.ajax({

    type:'get',

    url:"list/admin/discount",

    data:{},

      success:function(data){      
      //descuento 
      document.getElementById('value-discount-final').value = data.descuento;
      document.getElementById('apply-discount-final').value = 'no';
      var discount = '<button type="button"  class="btn btn-default"  onclick="incrementDiscount()">Des asignado: '+data.descuento+'%</button>' 
      $("#admin-discount").append(discount);  
      }

  
  });
  //clinete valor inicial
 document.getElementById("client-init").innerHTML = "Cliente: Sin seleccionar";   

  $(document).ready(function(){
    
   $("#selUser").select2({
    language: {
         noResults: function(term) {
          console.log(event.target.value);
             return "No results found.";
        }
    },
    width: '100%',
    allowClear: true,
    multiple: true,
    maximumSelectionSize: 1,
    placeholder: "Buscar cliente",
    ajax: { 
     url: "/admin/searh/client",
     type: "post",
     dataType: 'json',
     delay: 250,
     data: function (params) {
        
      return {
        searchTerm: params.term // search term
      };
     },
  
     processResults: function (data) {
        return {
            results: $.map(data, function (item) {
                return {
                    text: item.name,
                    id: item.id
                }
            })
        };
    },
     cache: true
    }
   });
});

  //cotizacion iniciar
  var discount = '<button type="button"  class="btn btn-default" onclick="selectQuotation(1)" id="select-quotation">Cotizacion</button>' 
  $("#seletQuotation").append(discount); 



 //panel detalles de selecion de bodega
 var table = $('#cellar-table').DataTable();
   
 $('#cellar-table tbody').on('click', 'button.select', function () {
   
  var dataIn = table.row($(this).parents('tr')).data();
  
     if ($('#div-cant-bod').length) {
       $("#div-cant-bod").remove();
       $("#total-cellar-bod-view").remove();
       $("#detaill-view-dat").remove();
       $("#detaill-view-dat-button").remove();
       $("#image-detaill").remove();
     }

   
   $.ajaxSetup({
     headers: {    
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
     }    
   });
   $.ajax({

     type:'POST',

     url:"list/cellar",

     data:{cod:dataIn.cod_cellar,idImage:dataIn.image},

       success:function(data){
         $("#list-cellar-view").append(data);
         var total = document.getElementById("total-cellar-bod").value; 
         var totalView =  '<p id="total-cellar-bod-view" style="border:1px solid #d4c4c4; "> Cantidad total de bodega:  '+ total +'</p>'
         $("#list-total-cellar-view").append(totalView);
         var image = document.getElementById("image-detaill").value;
         var valueDiscount =  document.getElementById("value-discount-unid"+dataIn.id).value
         var detaill = '<div class="col-xs-8" id="detaill-view-dat"><ul class="products-list product-list-in-box"><li class="item"><div class="product-img"><img id="cellar-image" src="/imagenes_productos/'+image+'" alt="Product Image"></div><div class="product-info"><a href="javascript:void(0)" class="product-title" > &nbsp;&nbsp '+dataIn.name+'</a><span class="product-description">&nbsp;&nbsp;&nbsp;'+dataIn.description+'<span></div></li><!-- /.item --></ul></div><div class="col-xs-4" id="detaill-view-dat-button"><button type="button" class="btn btn-block btn-success btn-lg" id="cellar-id-select" data-cellar-discount-unid="'+valueDiscount+'" data-cellar-name="'+dataIn.name+'" data-cellar-value="'+dataIn.value_sale+'" data-cellar-cod="'+dataIn.cod_cellar+'">Agregar</button></div>'
         $("#list-cellar-detaill").append(detaill);       
       }
   });        
 });

 $('#cellar-table tbody').on('click', 'button.discountUnid', function () {
  var client = document.getElementById("client-init").innerHTML; 

 
    var dataDiscount = table.row($(this).parents('tr')).data();
    
    if ($('#value-discount-unid'+dataDiscount.id).length) {
    var descuento = document.getElementById('value-discount-unid'+dataDiscount.id).value;
    }
    var descuentoInit = document.getElementById("value-discount-final").value

    if (descuento ==  descuentoInit) {
      var discountFinal = document.getElementById('value-discount-final').value
      document.getElementById('discount-unit'+dataDiscount.id).innerHTML = ''
      var discount = '<input type="hidden" id="value-discount-unid'+dataDiscount.id+'" value="'+discountFinal+'"> <button type="button" class="btn btn-default"  id="msm-status-discount-unid" >Aplicado &nbsp;<i class="fa fa-check"></i></button>'
      $("#discount-unit"+dataDiscount.id).append(discount);
    }else if (descuento > descuentoInit) {
      $.ajax({

        type:'post',
  
        url:"/admin/discount/request",
  
        data:{idClient:dataDiscount.id,discount:descuento,unid:1,cod_cellar:dataDiscount.cod_cellar},
  
          success:function(data){      
          //descuento 
            if (data.status) {
              document.getElementById('discount-unit'+dataDiscount.id).innerHTML = ''
              var discount = '<button type="button" class="btn btn-default" onClick="refreshStatusDsicountUnid('+data.id+','+dataDiscount.id+')" >'+data.msm+'&nbsp;<i class="fa fa-refresh"></i></button>'
              $("#discount-unit"+dataDiscount.id).append(discount)
            }else if (data.status == false) {
              alert(data.msm)
            }
        
          }
      });
    } 
 })
});


  //crear cliente
  function createClient(){   

    var formView = document.getElementById("client-view");  
    formView.style.display = "none";
   
    var formClient = document.getElementById("client-form");  
    formClient.style.display = "block";


   document.getElementById("alert_client").innerHTML = " ";
  }
  //campo buscar cliente seleccionar
   $('#selUser').on("select2:close", function (e) {
     
      var formView = document.getElementById("client-view");  
      formView.style.display = "none";

      var formClient = document.getElementById("client-form");  
      formClient.style.display = "block";

      var viewClientCredit = document.getElementById("client-credit-history");
      viewClientCredit.style.display = "none";

      var viewClientCredit = document.getElementById("table-credit-client");
      viewClientCredit.innerHTML = " ";

       //estado del cliente sin seleccion
       document.getElementById("client-init").innerHTML = "Cliente: Sin seleccionar"; 
       document.getElementById("alert_client").innerHTML = ''
       document.getElementById("all-credit-client").innerHTML = '';
   }) 

   //cliente buscar encontrado y seleccionado
  $('#selUser').on("select2:select", function (e) {
      var formClient = document.getElementById("client-form");  
      formClient.style.display = "none";
       var id = e.params.data.id;   
    $.ajax({

      type:'POST',

      url:"/admin/select/client",

      data:{id:id},

        success:function(data){
         document.getElementById("status_client").value = 1; 
         document.getElementById("client-view").style.display = "block"; 
         document.getElementById("type-document-client").innerHTML = data.type_document;
         document.getElementById("number-tdocument-client").innerHTML = data.num_document;
         document.getElementById("area-view-client").innerHTML = data.area;
         document.getElementById("mail-view-client").innerHTML = data.mail;
         var name = document.getElementById("name-view-client").innerHTML = data.name;
         var surname = document.getElementById("surname-view-client").innerHTML = data.Surname;         
         var ssurname = document.getElementById("ssurname-view-client").innerHTML = data.second_surname;
         document.getElementById("tradename-view-client").innerHTML = data.tradename;
         document.getElementById("addrers-view-client").innerHTML = data.addrers;
         document.getElementById("canton-view-client").innerHTML = data.province;
         document.getElementById("province-view-client").innerHTML = data.canton;
         document.getElementById("district-view-client").innerHTML = data.district;
         var credit = document.getElementById("district-client").innerHTML = data.credit;
         var alert = '<div class="alert alert-success alert-dismissible" ><strong>Super!</strong> El cliente<strong> ' +name+' '+surname+' '+ssurname+'</strong> fue agregado. </div>'
          $("#alert_client").append(alert)
          document.getElementById("id_client").value = id;
          creditHistoryClient(id)
          document.getElementById("client-init").innerHTML = "Cliente: " +name+' '+surname+' '+ssurname;  
        }
    });

 });

 function creditHistoryClient(idUser){
  $.ajax({

    type:'POST',

    url:"/admin/history/credit",

    data:{id:idUser},

      success:function(data){

        if(data.status){
          var viewClientCredit = document.getElementById("client-credit-history");
          viewClientCredit.style.display = "block";
          $("#table-credit-client").append(data.table)
          $("#all-credit-client").append(data.table_all)
        }
        
      }
  });


 }

   /*evento buscar*/
  $("#cellar-table_filter").addClass("hidden"); // hidden search input

  $("#search-cellar").on("input", function (e) {
    e.preventDefault();
    $('#cellar-table').DataTable().search($(this).val()).draw();
    if ($("#search-cellar").val() == '') {    
        $("#div-cant-bod").remove();
        $("#total-cellar-bod-view").remove();
        $("#detaill-view-dat").remove();
        $("#detaill-view-dat-button").remove();
    }
  });


  //evento seleccionar imagen
 $('body').on('click','#cellar-image', function(e){
    renderizarImagen(0)
 });

function pasarFoto() {
  var posicionActual = 0
  var cant =  document.getElementById('cant-img-cellar').value
    if(posicionActual >= cant - 1) {
        posicionActual = 0;
    } else {
        posicionActual++;
    }
        renderizarImagen(posicionActual);
}

function retrocederFoto() {
  var posicionActual = 0
  var cant =  document.getElementById('cant-img-cellar').value
    if(posicionActual <= 0) {
        posicionActual = 0;
    } else {
        posicionActual = cant - 1;
    }
    renderizarImagen(posicionActual);
}

function renderizarImagen (pos) {
  var idImage = document.getElementById('image-detaill-id').value 
  var modal = document.getElementById("myModal");
   modal.style.display = "block";
  $.ajaxSetup({
    headers: {    
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')    
    }    
  });
  $.ajax({

    type:'POST',

    url:"image/cellar",

    data:{id:idImage},
      success:function(data){
        var imagenInt = data
        var imagenIntCount = data.length
        document.getElementById('cant-img-cellar').value = imagenIntCount

        let $imagen = document.querySelector('#imagen-cellar');
    
        $imagen.style.backgroundImage = `url(${location.origin}/imagenes_productos/${imagenInt[pos]})`;

      }
    });
}






 //envia lista seleccionada--Lista bodega

$('body').on('click','#cellar-id-select', function(e){
  
  	var name = $(this).attr("data-cellar-name")
  	var value = $(this).attr("data-cellar-value")
  	var cod = $(this).attr("data-cellar-cod")

  	var valueDiscount = $(this).attr("data-cellar-discount-unid")

 	var nFilas = $("#list-cellar-selected tr").length;


 //iniciador de valor total
  if (document.getElementById('subtotal').innerHTML == '') { totalValueOne(value,1)  }else{ totalValueTwo(value,2) }


  var th = '<tr id="tr-list'+nFilas+'"><td>'+cod+'</td><td>'+name+'</td><td><input type="hidden" id="discount-unid-seleted" value="'+valueDiscount+'"><input type="hidden" id="value-one" value="'+value+'"><div class="carousel-cellar-an"><button  onClick="cantSelect(1,'+nFilas+')">+</button><input id="value-scroll'+nFilas+'" type="text" style="width : 25px;" value="1" disabled><button  onClick="cantSelect(2,'+nFilas+')" > - </button></div></td><td id="total-unit">'+value+'</td><td id="value-cellar'+nFilas+'">'+value+'</td><td><span class="glyphicon glyphicon-remove" onClick="removeCellarBill('+nFilas+')"></span></td></tr>'
  $("#list-cellar-selected").append(th);

  document.getElementById('list-cellar-detaill').innerHTML='';
  document.getElementById('list-cellar-view').innerHTML='';
  document.getElementById('list-total-cellar-view').innerHTML='';

  
});





function totalValueOne(val,init){


  var subtotal = document.getElementById('subtotal')

  //Actualizar subtotal
  subtotals = parseInt(val);
  subtotal.innerHTML = subtotals
 
  //actualizar descuento
  discountFinal = document.getElementById("apply-discount-final")//descuento geenral
  discountFinalUnid = document.getElementById("cellar-id-select") //decuento por unidad
  if (discountFinal.value == 'si') {
    var preDescuentoMult = parseInt(Math.round(subtotals))-((discountFinal.value * parseInt(subtotals))/100);
  	document.getElementById('descuento').innerHTML = (discountFinal.value * parseInt(subtotals))/100

  }else if(discountFinal.value == 'no' && discountFinalUnid.getAttribute('data-cellar-discount-unid') == 0){
  	var preDescuentoMult = parseInt(Math.round(subtotals))-( parseInt(subtotals)/100);
  	document.getElementById('descuento').innerHTML = (document.getElementById('descuento').innerHTML == 0)? 0 :document.getElementById('descuento').innerHTML
  }else if(discountFinalUnid.getAttribute('data-cellar-discount-unid') >= 1 ){

    var descuento = discountFinalUnid.getAttribute('data-cellar-discount-unid')
    var preDescuentoMult = parseInt(Math.round(subtotals))-((descuento * parseInt(subtotals))/100);
  	document.getElementById('descuento').innerHTML = (descuento * parseInt(subtotals))/100
  	
  }
   //Actualizar iva
  var ivaInit =  document.getElementById('iva')
  var valIva =  (preDescuentoMult * iva)/100
  ivaInit.innerHTML  = Math.round(valIva)

  //actualizar total
  var total = parseInt(Math.round(valIva)) + parseInt(Math.round(preDescuentoMult));
  document.getElementById('total').innerHTML = total
}


function totalValueTwo(val,init){



   //Actualizar subtotal
  var subtotal = document.getElementById('subtotal')
  var subtotals  = parseInt(val) + parseInt(subtotal.innerHTML);
  subtotal.innerHTML = subtotals 


  //actualizar descuento
  discountFinal = document.getElementById("apply-discount-final")//descuento geenral
  discountFinalUnid = document.getElementById("cellar-id-select") //decuento por unidad
  if (discountFinal.value == 'si') {
    var preDescuentoMult = parseInt(Math.round(subtotals))-((discountFinal.value * parseInt(subtotals))/100);
  	document.getElementById('descuento').innerHTML = (discountFinal.value * parseInt(subtotals))/100

  }else if(discountFinal.value == 'no' && discountFinalUnid.getAttribute('data-cellar-discount-unid') == 0){
  	var preDescuentoMult = parseInt(Math.round(subtotals))-( parseInt(subtotals)/100);
  	document.getElementById('descuento').innerHTML = (document.getElementById('descuento').innerHTML == 0)? 0 :document.getElementById('descuento').innerHTML
  }else if(discountFinalUnid.getAttribute('data-cellar-discount-unid') >= 1 ){

    var descuento = discountFinalUnid.getAttribute('data-cellar-discount-unid')
    var preDescuentoMult = parseInt(Math.round(subtotals))-((descuento * parseInt(subtotals))/100);
  	document.getElementById('descuento').innerHTML = (descuento * parseInt(subtotals))/100
  	
  }

   //Actualizar iva
  var ivaInit =  document.getElementById('iva')
  var valIva =  (preDescuentoMult * iva)/100
  ivaInit.innerHTML  = Math.round(valIva)

  //actualizar total
  var total = parseInt(Math.round(valIva)) + parseInt(Math.round(preDescuentoMult));
  document.getElementById('total').innerHTML = total
}




function cantSelect(select,row){

  if (document.getElementById("apply-discount-final").value == 'si') {
    var descuento = document.getElementById("value-discount-final").value
  }else if(document.getElementById("discount-unid-seleted").value >= 1 ){
    var descuento = document.getElementById("discount-unid-seleted").value;
  }else{
  	var descuento = 0;
  }
 
  var scroll = document.getElementById('value-scroll'+row)

    //aumentar precio
  var valueceld = document.getElementById('value-cellar'+row)
  var valueOne = document.getElementById('value-one')
  var subtotalVal = document.getElementById('subtotal')





  var increment = scroll.value;  
  if (select == 1) {
    increment++
    valueceld.innerHTML  = increment * valueOne.value
    subtotalVal.innerHTML = parseInt(valueOne.value) + parseInt(subtotalVal.innerHTML)

  }else if(select == 2){
    if (increment > 1) { 
      increment--
       valueceld.innerHTML  = increment * valueOne.value
       subtotalVal.innerHTML = parseInt(valueOne.value) - parseInt(subtotalVal.innerHTML)
    }    
   
  }


  scroll.value = increment;

  //actualizar subtotal

  var nFilas = $("#list-cellar-selected tr").length;

  var count = nFilas -1;
  var total = 0
   for (var i = 1; i <= count; i++) {
      //actualizar subtotal  
        totalValue =  document.getElementById('value-cellar'+ i );
        total += parseInt(totalValue.innerHTML);       
   }
    var subtotalVal = document.getElementById('subtotal').innerHTML = parseInt(total)

    //actualizar descuento  
    var preDescuentoMult =  parseInt(Math.round(total))-((descuento * parseInt(total))/100);
    document.getElementById('descuento').innerHTML = ((descuento * parseInt(total))/100);
    
    // actualizar iva 
    var init = document.getElementById('iva')       
    var valIva =  (parseInt(preDescuentoMult) * iva)/100
    init.innerHTML  = Math.round(valIva)

    //Actualizar total
    var total = parseInt(Math.round(valIva)) + parseInt(Math.round(preDescuentoMult)) ;
    document.getElementById('total').innerHTML = total


}

function removeCellarBill(row){
  var descuento = document.getElementById("value-discount-final").value
  var value = document.getElementById('value-cellar'+row).innerHTML
  var subtotal = document.getElementById('subtotal')

  //Actualizar subtotal
  subtotals = parseInt(subtotal.innerHTML) - parseInt(value);
  subtotal.innerHTML = subtotals 
 
  //actualizar descuento
  var discount = document.getElementById('descuento')

  var preDescuentoMult = parseInt(Math.round(subtotals))-((descuento * parseInt(subtotals))/100);
  discount.innerHTML = (descuento * parseInt(subtotals))/100

   //Actualizar iva
  var ivaInit =  document.getElementById('iva')
  var valIva =  (preDescuentoMult * iva)/100
  ivaInit.innerHTML  = Math.round(valIva)

  //actualizar total
  var total = parseInt(Math.round(valIva)) + parseInt(Math.round(preDescuentoMult));
  document.getElementById('total').innerHTML = total
  $("#tr-list"+row).remove();
}



//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass   : 'iradio_minimal-blue'
})  


$('#submit-sale').on('click', function(e) {
 if (document.getElementById('id_client').value == '') {
  	alert('El decuento supera a lo predeterminado ')	
  		
  }

  e.preventDefault();
var id_client = document.getElementById('id_client').value
var coti_venta = document.getElementById('select-quotation').innerHTML
var medio_factura = document.getElementById("type-bill").value;

  var tbl = $('#list-cellar-selected tr').map(function(rowIdx, row) {
    
      var rowObj = $(row).find('td').map(function(cellIdx, cell) {
          var retVal = {};
          retVal['cell' + cellIdx] = cell.textContent.trim();
          return retVal;
      }).get();
      var retVal = {};
      retVal['row' + rowIdx] = rowObj;


     
      return retVal;
  }).get();
   var obj = '{'
       +'"id_client" : "'+id_client+'",'
       +'"coti_venta"  : "'+coti_venta+'",'
       +'"medio_factura"  : "'+medio_factura+'"'
       +'}';
 
  //console.log('-->' + JSON.stringify({table: tbl}));

  $.post('/process/sale', {table: tbl,data:obj}, function(data, textStatus, jqXHR) {
      console.log(data);
  })
});

