// Get the modal
var modal = document.getElementById("myModal");
var modalClient = document.getElementById("modal-client");


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close-image")[0];
var spanClient = document.getElementsByClassName("close-client")[0];
var spanCredit = document.getElementsByClassName("close-credit")[0];

// When the user clicks the button, open the modal 
function verImage() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

spanClient.onclick = function() {
  modalClient.style.display = "none";
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
  
   if (event.target == modalClient) {
    modalClient.style.display = "none";
  }  
}