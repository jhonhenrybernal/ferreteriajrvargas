<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    //protected $table = 'cellar_Location';
    public $timestamps = true;


    protected $fillable = [
    	'id',
    	'type_document',
    	'num_document',
    	'area',
    	'mail',
    	'name',
    	'Surname',
    	'second_surname',
    	'tradename',	
    	'addrers',
    	'province',	
    	'canton',
    	'district',	
    	'credit',
        'created_at',
        'updated_at'
    ];

  

}
