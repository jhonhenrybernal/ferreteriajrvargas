<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discounts extends Model
{
   protected $fillable = [
    	'id',
    	'id_user',	
    	'id_client',	
    	'status',	
    	'bill',	
		'created_at',
		'cod_cellar'
    ];
}
