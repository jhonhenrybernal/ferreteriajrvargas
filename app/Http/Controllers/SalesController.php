<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cellar;
use App\Clients;
use App\ImageProduct;
use App\CellarLocation;
use App\creditUsers;
use App\creditStories;
use App\Discounts;
use App\User;
use Auth;
use Yajra\DataTables\Datatables;
use Illuminate\Support\Facades\DB;

class SalesController extends Controller
{
    public function index(){
        return view('sales.index');
    }


    public function sspCellarAll(Request $data){

    $data = $data->all('search'); 
    $dataFilter = $data['search']['value'];
    
    if ($dataFilter == null) {
        $cellarAll = [];
    }else if(!empty($dataFilter)){
        $cellarAll = Cellar::all();
    }

        return Datatables::of($cellarAll)->addColumn('action', function ($cellarAll) {                             

        })->make(true);
    }

    public function sspListCellar(Request $cod){
        $cellar = CellarLocation::where('cod_cellar',$cod->cod)->get();
        $imageOne = ImageProduct::where('id_cellar',$cod->idImage)->first();

        $total = 0;
        foreach($cellar as $row){
            $total +=  $row['quantity'];
        }
        $output = '';
        $output  .= '<div class="margin" id="div-cant-bod" style="margin: 20px;">';
        foreach($cellar as $row){
            $output .= '<div class="btn-group">
                                    <p>Bodega:'.$row['location'].'</p>
                            <input type="text"  lass="btn btn-default" value="Cantidad: '.$row['quantity'].'" style="width: 90px" disabled="">                           
                            </div> 
                        ';
        }

        $output  .= '</div>
          <input type="hidden" id="id-cellar-bod" value="'. $cod->id.'">
          <input type="hidden" id="total-cellar-bod" value="'.$total.'">
          <input type="hidden" id="image-detaill" value="'.$imageOne['name'].'">
          <input type="hidden" id="image-detaill-id" value="'.$imageOne['id_cellar'].'"></div>'
          ;

         return response()->json($output);
    }

    public function sspImageCellar(Request $id){

        $image = ImageProduct::where('id_cellar',$id->id)->get();

        $output = '';
        foreach ($image as $key => $img) {
            $list[] = $img['name'];
        }
        
        return response()->json($list);

    }

    public function sspAdminDiscount(){    
        $user = User::find(Auth::id());
        $admin = [
            'descuento' => $user['discount'],
            ];
        return response()->json($admin);
    }

    /*public function sspClient(){
        $client = Clients::all();
        return response()->json($client);
    }

     public function sspClientSelect($id){
        $client = Clients::find($id);
        return response()->json($client);
    }*/

    public function sspDiscoutRequest(Request $request){
        //1 en espera, 2 aprobado, 3 no aprovado
        $data = $request->all();
        $user = User::find(Auth::id());

        $discount = new Discounts();
        $discount->id_user = Auth::id();
        $discount->cod_cellar = ($data['idClient'] == null)?'cot':$data['cod_cellar'];
        $discount->id_client = $data['idClient'];
        $discount->status = 1;
        $discount->bill = 'N/A';
        $discount->value_current =  $data['discount'];
        $discount->save();

        //por el momento true para estatus 
        $result = [
            'status' => (empty($discount['id']))?false:true,
            'id' => $discount['id'],
            'msm' => 'Por aprovar...'];
        return response()->json($result);
    }

    public function sspSearchClient(Request $data){
        $search = $data->all();
        $client = DB::table('clients')
                ->where('num_document', 'like', $search['searchTerm'].'%')
                ->orWhere('name', 'like', $search['searchTerm'].'%')
                ->orWhere('Surname', 'like', $search['searchTerm'].'%')
                ->orWhere('second_surname', 'like', $search['searchTerm'].'%')
                ->get();
        $result = array();
        foreach (json_decode($client,true) as $key => $value) {
            $result[] = [
                'id' => $value['id'],
                'name'=> $value['name'].' '.$value['Surname'].' '.$value['second_surname']                
            ];
        } 
 
      
        return response()->json($result);
    }

    public function sspClientSelect(Request $id){
        $id = $id->all();
        $client = DB::table('clients')->where('id',$id['id'])->first();
        return response()->json($client);
    }

    public function sspClientCreate(Request $data){
        $datas = $data->all();

        try {
            
            $client = new Clients();
            $client->type_document = $data['tipoDoc'];
            $client->num_document = $data['NumDocu'];
            $client->area = $data['area'];
            $client->mail  = $data['mail'];
            $client->name  = $data['name'];
            $client->Surname  = $data['surname'];
            $client->second_surname = $data['ssurname'];
            $client->tradename = $data['tradename'] ;
            $client->addrers = $data['addrers'];
            $client->province = $data['province'] ;
            $client->canton = $data['cantor'];
            $client->district = $data['district'] ;
            $client->credit = 0;
            $client->save();
            $fullname = $data['name']. ' ' .$data['surname'].' ' . $data['ssurname'];

            $return = [
                'result' => true,
                'name_full' => $fullname,
                'id' => $client['id']
            ];
        } catch (Exception $e) {
            $return = [
                'result' => false,
            ];
        }

        return response()->json($return);
    }

    public function sspDiscountStatus(Request $data){
        $id = $data->all();
        $discount = Discounts::find($id['id']);

        switch ($discount->status) {
            case 1:
                $result = ['status' => 1,'msm' => 'Por aprovar...', 'discount' => $discount->value_current];
                break;
            case 2:
                $result = ['status' => 2,'msm' => 'Aprovado...', 'discount' => $discount->value_current];
                break;
            case 3:
                $result = ['status' => 3,'msm' => 'Rechazado...', 'discount' => $discount->value_current];
                break;
        }
       return response()->json($result);
    }
    
    public function sspClientCreditHistory(Request $request){
        $clientId = $request->all();
        $credit = creditStories::where('id_user',$clientId['id'])->get();
        $status = (creditStories::where('id_user',$clientId['id'])->count() == 0)?false:true; 
       
        $table = '';
        $table  .= '<table class="table table-condensed">
                        <tr>
                            <th># factura</th>
                            <th>Saldo</th>
                            <th>Disponible</th>
                            <th>Total de credito</th>
                        </tr>
                       ';
                        $creditInit = 0;
                        $creditRequered = 0;
                        $creditBalance = 0;
                        foreach ($credit as $row) {
                            $creditInit += $row['credit_init'];
                            $creditRequered += $row['credit_requested'];
                            $creditBalance += $row['credit_balance']; 

                            $table .= ' <tr>
                                        <td>'. $row['num_bill'].'</td>';
                            $table .= '<td>'.$row['credit_init'].'</td>';
                            $table .= '<td>'.$row['credit_requested'].'</td>';
                            $table .= '<td>'.$row['credit_balance'].'</td>';
                            $table .= '<td>'.$row['created_at'].'</td> 
                                        </tr>';
                        }
                    
                          
        $table .= '      
                    </table>';

        $allCredit = '';            
        $allCredit  .= ' <div class="btn-group">
                            <button type="text" class="btn btn-default" id="client-balance" >Saldo: '.$creditInit.'</button>
                        </div>
                        <div class="btn-group">
                            <button type="text" class="btn btn-default" id="client-balance" >Disponible:'.$creditRequered.'</button>
                        </div>
                        <div class="btn-group">
                            <button type="text" class="btn btn-default" id="client-balance" >Total de credito:'.$creditBalance.'</button>
                        </div>  ';
 
        $result = ['status' => $status,
                    'table_all' => $allCredit, 
                    'table' => $table];            
        

        return response()->json($result);
    }

    public  function saleProcess(Request $request){
        dd($request);
    }
}