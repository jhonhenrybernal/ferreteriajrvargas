<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Yajra\DataTables\Datatables;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function sspIndex(){
        
        $product = Product::groupBy('cellar_id')->get();

        return Datatables::of($product)->addColumn('action', function ($product) {                             

        })->make(true);


        

    }
}
