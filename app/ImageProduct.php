<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageProduct extends Model
{
    protected $table = 'image_product';
    public $timestamps = false;

    protected $fillable = [
        'id', 'name','id_cellar'
    ];
}
