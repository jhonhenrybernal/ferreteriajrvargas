<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class creditStories extends Model
{
   protected $table = 'credit_stories';
 	protected $fillable = [
    	'id',	
    	'id_user',	
    	'num_bill',	
    	'credit_init',	
    	'credit_requested',	
    	'credit_balance',	
    	'created_at'
    ];
}
