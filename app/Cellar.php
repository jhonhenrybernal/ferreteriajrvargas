<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cellar extends Model
{
    protected $table = 'cellar';
    public $timestamps = false;

    protected $fillable = [
        'id', 'cod_cellar','name','description', 'image', 'value_sale','cellar_Location_id','value_public','cant','iva'
    ];

    public function imagesOne(){
    	return $this->hasOne('App\ImageProduct');
    }
}
