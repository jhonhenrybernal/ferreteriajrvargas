<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //protected $table = 'products';
    public $timestamps = false;

    protected $fillable = [
        'id', 'name','description', 'image', 'value','cellar_id'
    ];
}
