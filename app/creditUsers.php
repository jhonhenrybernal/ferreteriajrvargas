<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class creditUsers extends Model
{
	protected $table = 'credit_create';
 	protected $fillable = [
    	'id_user',
    	'value_init',
    	'value_current',
    	'created_at'
    ];
}
