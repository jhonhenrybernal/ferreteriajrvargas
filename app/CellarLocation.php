<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CellarLocation extends Model
{
    protected $table = 'cellar_Location';
    public $timestamps = false;

    protected $fillable = [
        'id', 'cod_cellar','location','	quantity'
    ];
}
